/*
 * Copyright (C) 2011-2018 Vinay Sajip. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _WIN32_WINNT            // Specifies that the minimum required platform is Windows Vista.
#define _WIN32_WINNT 0x0600     // Change this to the appropriate value to target other versions of Windows.
#endif

#include <stdio.h>
#include <windows.h>
#include <Shlwapi.h>

// We want to be able to link to just python3.dll
#define Py_LIMITED_API

#ifdef _DEBUG
#define DEBUG
#undef _DEBUG
#endif
#include "Python.h"
#ifdef DEBUG
#define _DEBUG
#endif // _DEBUG


#pragma comment (lib, "Shlwapi.lib")

#define USE_ENVIRONMENT
#define SUPPORT_RELATIVE_PATH

#define MSGSIZE 1024

static wchar_t suffix[] = {
#if defined(_CONSOLE)
    L"-script.py"
#else
    L"-script.pyw"
#endif
};

static void
assert_(BOOL condition, char * format, ...)
{
	if (!condition) {
		va_list va;
		char message[MSGSIZE];
		int len;

		va_start(va, format);
		len = vsnprintf_s(message, MSGSIZE, MSGSIZE - 1, format, va);
#if defined(_CONSOLE)
		fprintf(stderr, "Fatal error in launcher: %s\n", message);
#else
		MessageBoxTimeoutA(NULL, message, "Fatal Error in Launcher",
			MB_OK | MB_SETFOREGROUND | MB_ICONERROR,
			0, 3000);
#endif
		ExitProcess(1);
	}
}


// We delay loading python3.dll to attempt to load from folder of shebang python
#include <delayimp.h> 
static wchar_t * dll_dir = NULL;

FARPROC WINAPI delayHook(unsigned dliNotify, PDelayLoadInfo pdli)
{
	if (dliNotify == dliNotePreLoadLibrary &&
		strcmp(pdli->szDll, "python3.dll") == 0)
	{
		if (dll_dir != NULL) {
			SetDllDirectory(dll_dir);
			HMODULE hLib = LoadLibrary(L"python3.dll");
			if (hLib == NULL) {
				char c_dll_path[MAX_PATH];
				wcstombs(c_dll_path, dll_dir, sizeof(c_dll_path));
				assert_(hLib != NULL, "Could not find python3.dll in %s", c_dll_path);
			}
			return (FARPROC)(hLib);
		}
	}
	return NULL;
}
const PfnDliHook __pfnDliNotifyHook2 = delayHook;


static int pid = 0;

#if !defined(_CONSOLE)

typedef int (__stdcall *MSGBOXWAPI)(IN HWND hWnd,
        IN LPCSTR lpText, IN LPCSTR lpCaption,
        IN UINT uType, IN WORD wLanguageId, IN DWORD dwMilliseconds);

#define MB_TIMEDOUT 32000

int MessageBoxTimeoutA(HWND hWnd, LPCSTR lpText,
    LPCSTR lpCaption, UINT uType, WORD wLanguageId, DWORD dwMilliseconds)
{
    static MSGBOXWAPI MsgBoxTOA = NULL;
    HMODULE hUser = LoadLibraryA("user32.dll");

    if (!MsgBoxTOA) {
        if (hUser)
            MsgBoxTOA = (MSGBOXWAPI)GetProcAddress(hUser,
                                      "MessageBoxTimeoutA");
        else {
            /*
             * stuff happened, add code to handle it here
             * (possibly just call MessageBox())
             */
        }
    }

    if (MsgBoxTOA)
        return MsgBoxTOA(hWnd, lpText, lpCaption, uType, wLanguageId,
                         dwMilliseconds);
    if (hUser)
        FreeLibrary(hUser);
    return 0;
}

#endif


static wchar_t script_path[MAX_PATH];
FILE * fp = NULL;

#define LARGE_BUFSIZE (65 * 1024 * 1024)

typedef struct {
    DWORD sig;
    DWORD unused_disk_nos;
    DWORD unused_numrecs;
    DWORD cdsize;
    DWORD cdoffset;
} ENDCDR;

/* We don't want to pick up this variable when scanning the executable.
 * So we initialise it statically, but fill in the first byte later.
 */
static char
end_cdr_sig [4] = { 0x00, 0x4B, 0x05, 0x06 };

static char *
find_pattern(char *buffer, size_t bufsize, char * pattern, size_t patsize)
{
    char * result = NULL;
    char * p;
    char * bp = buffer;
    size_t n;

    while ((n = bufsize - (bp - buffer) - patsize) >= 0) {
        p = (char *) memchr(bp, pattern[0], n);
        if (p == NULL)
            break;
        if (memcmp(pattern, p, patsize) == 0) {
            result = p; /* keep trying - we want the last one */
        }
        bp = p + 1;
    }
    return result;
}

static char *
find_shebang(char * buffer, size_t bufsize)
{
    errno_t rc;
    char * result = NULL;
    char * p;
    size_t read;
    long pos;
    __int64 file_size;
    __int64 end_cdr_offset = -1;
    ENDCDR end_cdr;

    rc = _wfopen_s(&fp, script_path, L"rb");
    assert_(rc == 0, "Failed to open executable");
    fseek(fp, 0, SEEK_END);
    file_size = ftell(fp);
    pos = (long) (file_size - bufsize);
    if (pos < 0)
        pos = 0;
    fseek(fp, pos, SEEK_SET);
    read = fread(buffer, sizeof(char), bufsize, fp);
    p = find_pattern(buffer, read, end_cdr_sig, sizeof(end_cdr_sig));
    if (p != NULL) {
        end_cdr = *((ENDCDR *) p);
        end_cdr_offset = pos + (p - buffer);
    }
    else {
        /*
         * Try a larger buffer. A comment can only be 64K long, so
         * go for the largest size.
         */
        char * big_buffer = malloc(LARGE_BUFSIZE);
        int n = (int) LARGE_BUFSIZE;

        pos = (long) (file_size - n);

        if (pos < 0)
            pos = 0;
        fseek(fp, pos, SEEK_SET);
        read = fread(big_buffer, sizeof(char), n, fp);
        p = find_pattern(big_buffer, read, end_cdr_sig, sizeof(end_cdr_sig));
		if (p == NULL) {
			free(big_buffer);
			return NULL;
		}
		end_cdr = *((ENDCDR *) p);
        end_cdr_offset = pos + (p - big_buffer);
        free(big_buffer);
    }
    end_cdr_offset -= end_cdr.cdsize + end_cdr.cdoffset;
    /*
     * end_cdr_offset should now be pointing to the start of the archive,
     * i.e. just after the shebang. We'll assume the shebang line has no
     * # or ! chars except at the beginning, and fits into bufsize (which
     * should be MAX_PATH).
     */
    pos = (long) (end_cdr_offset - bufsize);
    if (pos < 0)
        pos = 0;
    fseek(fp, pos, SEEK_SET);
    read = fread(buffer, sizeof(char), bufsize, fp);
	fseek(fp, (long)(-1 * bufsize), SEEK_CUR);
    assert_(read > 0, "Unable to read from file");
    p = &buffer[read - 1];
    while (p >= buffer) {
        if (memcmp(p, "#!", 2) == 0) {
            result = p;
            break;
        }
        --p;
    }
    return result;
}


#if defined(USE_ENVIRONMENT)
/*
 * Where to place any executable found on the path. Should be OK to use a
 * static as there's only one of these per invocation of this executable.
 */
static wchar_t path_executable[MSGSIZE];

static BOOL find_on_path(wchar_t * name)
{
    wchar_t * pathext;
    size_t    varsize;
    wchar_t * context = NULL;
    wchar_t * extension;
    DWORD     len;
    errno_t   rc;
    BOOL found = FALSE;

    if (wcschr(name, L'.') != NULL) {
        /* assume it has an extension. */
        if (SearchPathW(NULL, name, NULL, MSGSIZE, path_executable, NULL))
            found = TRUE;
    }
    else {
        /* No extension - search using registered extensions. */
        rc = _wdupenv_s(&pathext, &varsize, L"PATHEXT");
        _wcslwr_s(pathext, varsize);
        if (rc == 0) {
            extension = wcstok_s(pathext, L";", &context);
            while (extension) {
                len = SearchPathW(NULL, name, extension, MSGSIZE, path_executable, NULL);
                if (len) {
                    found = TRUE;
                    break;
                }
                extension = wcstok_s(NULL, L";", &context);
            }
            free(pathext);
        }
    }
    return found;
}

/*
 * Find an executable in the environment. For now, we just look in the path,
 * but potentially we could expand this to look in the registry, etc.
 */
static wchar_t *
find_environment_executable(wchar_t * line) {
    BOOL found = find_on_path(line);

    return found ? path_executable : NULL;
}

#endif

static wchar_t *
skip_ws(wchar_t *p)
{
    while (*p && iswspace(*p))
        ++p;
    return p;
}

static wchar_t *
skip_me(wchar_t * p)
{
    wchar_t * result;
    wchar_t terminator;

    if (*p != L'\"')
        terminator = L' ';
    else {
        terminator = *p++;
        ++p;
    }
    result = wcschr(p, terminator);
    if (result == NULL) /* perhaps nothing more on the command line */
        result = L"";
    else
        result = skip_ws(++result);
    return result;
}

static char *
find_terminator(char *buffer, size_t size)
{
    char c;
    char * result = NULL;
    char * end = buffer + size;
    char * p;

    for (p = buffer; p < end; p++) {
        c = *p;
        if (c == '\r') {
            result = p;
            break;
        }
        if (c == '\n') {
            result = p;
            break;
        }
    }
    return result;
}

static wchar_t *
find_exe_extension(wchar_t * line) {
    wchar_t * p;

    while ((p = StrStrIW(line, L".exe")) != NULL) {
        wchar_t c = p[4];

        if ((c == L'\0') || (c == L'"') || iswspace(c))
            break;
        line = &p[4];
    }
    return p;
}

static wchar_t *
set_dll_dir(wchar_t * line) {
	
	if (PathRemoveFileSpec(line)) {
		dll_dir = line;
	}
	return line;
}

static wchar_t *
find_exe(wchar_t * line, wchar_t ** argp)
{
    wchar_t * p = find_exe_extension(line);
#if defined(USE_ENVIRONMENT)
    wchar_t * q;
    int n;
#endif
    wchar_t * result;

#if !defined(USE_ENVIRONMENT)
    assert_(p != NULL, "Expected to find a command ending in '.exe' in shebang line: %ls", line);
    p += 4; /* skip past the '.exe' */
    result = line;
#else
    if (p != NULL) {
        p += 4; /* skip past the '.exe' */
        result = line;
    }
    else {
        n = _wcsnicmp(line, L"/usr/bin/env", 12);
        assert_(n == 0, "Expected to find a command ending in '.exe' in shebang line: %ls", line);
        p = line + 12; /* past the '/usr/bin/env' */
        assert_(*p && iswspace(*p), "Expected to find whitespace after '/usr/bin/env': %ls", line);
        do {
            ++p;
        } while (*p && iswspace(*p));
        /* Now, p points to what comes after /usr/bin/env and any following whitespace. */
        q = p;
        /* Skip past executable name and NUL-terminate it. */
        while (*q && !iswspace(*q))
            ++q;
        if (iswspace(*q))
            *q++ = L'\0';
        result = find_environment_executable(p);
        //assert_(result != NULL, "Unable to find executable in environment: %ls", line);
        p = q; /* point past name of executable in shebang */
    }
#endif
    if (*line == L'"') {
        assert_(*p == L'"', "Expected terminating double-quote for executable in shebang line: %ls", line);
        *p++ = L'\0';
        ++line;
        ++result;  /* See https://bitbucket.org/pypa/distlib/issues/104 */
    }
    /* p points just past the executable. It must either be a NUL or whitespace. */
    assert_(*p != L'"', "Terminating quote without starting quote for executable in shebang line: %ls", line);
    /* if p is whitespace, make it NUL to truncate 'line', and advance */
    if (*p && iswspace(*p))
        *p++ = L'\0';
    /* Now we can skip the whitespace, having checked that it's there. */
    while(*p && iswspace(*p))
        ++p;
    *argp = p;
    return result;
}

static int
process(int argc, wchar_t * argv[])
{
    wchar_t * cmdline = skip_me(GetCommandLineW());
    wchar_t * psp;

	static char c_script_path[MAX_PATH];

	size_t len = GetModuleFileNameW(NULL, script_path, MAX_PATH);
	GetModuleFileNameA(NULL, c_script_path, MAX_PATH);

    char buffer[MAX_PATH];
    wchar_t wbuffer[MAX_PATH];
#if defined(SUPPORT_RELATIVE_PATH)
    wchar_t dbuffer[MAX_PATH];
    wchar_t pbuffer[MAX_PATH];
#endif
    char *cp;
    wchar_t * wcp;
    char * cmdp;
    char * p;
    wchar_t * wp;
    int n;
    errno_t rc;
	
    if (script_path[0] != L'\"')
        psp = script_path;
    else {
        psp = &script_path[1];
        len -= 2;
    }
    psp[len] = L'\0';

	/* Initialise signature dynamically so that it doesn't appear in
	* a stock executable.
	*/
	end_cdr_sig[0] = 0x50;

	p = find_shebang(buffer, MAX_PATH);
	
	//assert_(p != NULL, "Failed to find shebang");
	
	if (p == NULL) {
		/* Replace the .exe with -script.py(w) */
		wp = wcsstr(psp, L".exe");
		assert_(wp != NULL, "Failed to find \".exe\" in executable name");

		len = MAX_PATH - (wp - script_path);
		assert_(len > sizeof(suffix), "Failed to append \"%ls\" suffix", suffix);
		wcsncpy_s(wp, len, suffix, sizeof(suffix));

		wcstombs(c_script_path, psp, sizeof(c_script_path));

		rc = _wfopen_s(&fp, psp, L"rb");
		assert_(rc == 0, "Failed to open script file '%ls'", psp);
		fread(buffer, sizeof(char), MAX_PATH, fp);
		fclose(fp);
		p = buffer;
	}

    cp = find_terminator(p, MAX_PATH);
    assert_(cp != NULL, "Expected to find terminator in shebang line");
    *cp = '\0';

	// Decode as UTF-8
    n = MultiByteToWideChar(CP_UTF8, MB_ERR_INVALID_CHARS, p, (int) (cp - p), wbuffer, MAX_PATH);
    assert_(n != 0, "Expected to decode shebang line using UTF-8");
    wbuffer[n] = L'\0';
    wcp = wbuffer;
    while (*wcp && iswspace(*wcp))
        ++wcp;
    assert_(*wcp == L'#', "Expected to find \'#\' at start of shebang line");
    ++wcp;
    while (*wcp && iswspace(*wcp))
        ++wcp;
    assert_(*wcp == L'!', "Expected to find \'!\' following \'#\' in shebang line");
    ++wcp;
    while (*wcp && iswspace(*wcp))
        ++wcp;
    wp = NULL;
    wcp = find_exe(wcp, &wp);

#if defined(SUPPORT_RELATIVE_PATH)
	/*
	If the executable is not just "python.exe" and has a relative path, resolve it
	relative tot this executable.
	*/
#if 1
	if (_wcsnicmp(L"python.exe", wcp, 10) && PathIsRelativeW(wcp)) {
		wcscpy_s(dbuffer, MAX_PATH, script_path);
		PathRemoveFileSpecW(dbuffer);
		PathCombineW(pbuffer, dbuffer, wcp);  // appears to canonicalize the path
		if (!PathFileExistsW(pbuffer)) {
			GetCurrentDirectoryW(MAX_PATH, dbuffer);
			PathCombineW(pbuffer, dbuffer, wcp);  // appears to canonicalize the path
		}
		wcp = pbuffer;
	}
#endif
#endif
	// If the executable has been found from shebang, set its directory as the default dll load path.
	set_dll_dir(wcp);
	
	int errorCode = 0;
	/* Pass argv[0] to the Python interpreter */
	Py_SetProgramName(argv[0]);

	/* Initialize the Python interpreter.  Required. */
	Py_Initialize();

	PySys_SetArgvEx(argc, argv, 0);

	PyObject* glb;
	PyObject* loc;
	PyObject *module = PyImport_ImportModule("__main__");
	loc = glb = PyModule_GetDict(module);

	char cmd[] = "import runpy;runpy.run_path(r\"%s\",run_name=\"__main__\")\n";
	len = strlen(c_script_path) + sizeof(cmd);
	cmdp = (char *)calloc(len, sizeof(char));
	assert_(cmdp != NULL, "Expected to be able to allocate command line memory");
	_snprintf_s(cmdp, len, len, cmd, c_script_path);

	PyObject* py_cmd = Py_CompileString(cmdp, c_script_path, Py_single_input);
	PyObject* py_res = PyEval_EvalCode(py_cmd, glb, loc);

	free(cmdp);

	Py_XDECREF(py_res);
	Py_XDECREF(py_cmd);
	if (PyErr_Occurred())
	{
		errorCode = 1;
		PyErr_Print();
	}

	//PyRun_SimpleFileExFlags(fp, c_script_path, 1, NULL);

	Py_Finalize();

	Py_Exit(errorCode);
	return errorCode;
}

#if defined(_CONSOLE)

int wmain(int argc, wchar_t** argv)
{
    return process(argc, argv);
}

#else

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                     LPSTR lpCmdLine, int nCmdShow)
{
	LPWSTR *szArglist;
	int nArgs;
	szArglist = CommandLineToArgvW(GetCommandLine(), &nArgs);

	int ret = process(nArgs, szArglist);
	
	LocalFree(szArglist);
	return ret;
}

#endif
